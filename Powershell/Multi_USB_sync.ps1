$host.ui.RawUI.WindowTitle = "USB Drive Parallel Sync Tool"

# The minumum size of a drive in GB that this tool will allow.
$minsize = 4

# The source directory to pull from
$source = "\\path to source"

# This is the filesystem label
$date = Get-Date -f (get-date -Format M-y )
$label = "USB " + $date

function Sync-Drives {
    param([array]$Letters, [string]$SyncSource)
    foreach ($item in $Letters) {
        Write-Host "Syncing" $item "drive"
        $item = $item + ":\"
        # We run chkdsk here because sometimes windows will complain that the filesystem has errors the next time we plug the drive in. Hopefully, this should prevent that pop up.
        # chkdsk exit codes explained here: https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/chkdsk#understanding-exit-codes
        Start-Process powershell.exe -PassThru -ArgumentList "robocopy",$SyncSource,$item,"/MIR","/v","/W:5","/ndl;","Write-Host","Drive - ",$item,"`;",`
        "if (`$LASTEXITCODE -gt 8){Read-Host `"ERRORS Reported! Something went Wrong with this sync job!``n Press any key to exit`"}","`;",`
        "Write-Host","This window will close on its own in a moment``nChecking filesystem for errors...","`;",`
        "chkdsk.exe",$item,"/markclean","|","Out-Null","`;",`
        "if (`$LASTEXITCODE -eq 3){Read-Host `"Filesystem errors were found!``nUse chkdsk.exe to repair the drive, or try reformatting`"}" | Out-Null
    }
    foreach ($item in $Letters) {
        # Here you can place an additional command to run on each drive if needed
    }
}

function Get-Capacity {
    param ([array]$cap, [int]$min)
    # This number is used to convert bytes to GB
    $mod = 1024 * 1024 * 1024
    $cap = $cap | ForEach-Object {$_ / $mod}
    $i = 0
    foreach ($item in $cap) {
        if ($item -lt $min){
            $i = $i + 1
        }
    }
    if ($i -gt 0) {
        Read-Host $i" drive[s] or partition[s] are too small.`nPlease ensure that all USB drives are at least" $min"GB in capacity.`nPress any key to exit"
    exit
    }
}

Write-Host "Please select what you want to do:

1) Update existing synced drives
2) Crete new synced drives`n"

$option = "placeholder"
while ("1","2" -notcontains $option){
    $option = Read-Host "Your selection"
}

if ($option -eq "1") {
    # DriveType 2 is for removeable drives, should only apply to USB drives... hopefully :P
    # https://docs.microsoft.com/en-us/dotnet/api/system.io.drivetype?view=net-5.0
    $partitions = Get-CimInstance Win32_Volume -Filter "DriveType='2'"
    $list = [string]""
    foreach ($item in $partitions.DriveLetter) {
        $list = $list + ($item -replace ':','')
    }
    if ($list.length -lt 1) {
        Read-Host "No removeable drives detected!`nPress any key to exit"
        exit
    }
    
    [array]$capacity = $partitions.Capacity
    Get-Capacity -cap $capacity -min $minsize

    Write-Output "`nThe following drives will be used" $partitions | Format-Table DriveLetter,Label,FileSystem
    
    $answer = "placeholder"
    while ("y","n" -notcontains $answer) {
        $answer = Read-Host "Is this okay? [Y] or [N]"
    }
    if ($answer -eq "n") {
        Read-Host "Quitting...`nPress any key to exit"
        exit
    }
    if ($answer -eq "y") {
        foreach ($item in $partitions.DriveLetter) {
            $Letter = $item -replace ':',''
            Set-Volume -NewFileSystemLabel $label -DriveLetter $Letter
        }
    }
}

if ($option -eq "2") {
    $usb = Get-Disk | Where-Object -FilterScript {$_.Bustype -Eq "USB"}
    $i = 0
    foreach ($item in $usb.Number) {
        $i = $i + 1
    }
    if ($i -lt 1) {
        Read-Host "No removeable drives detected!`nPress any key to exit"
        exit
    }
    
    [array]$capacity = $usb.Size
    Get-Capacity -cap $capacity -min $minsize
    
    Write-Output "`nThe following drives will be formatted" $usb
    
    $answer = "placeholder"
    while ("y","n" -notcontains $answer) {
        $answer = Read-Host "`nDo you want to format the drives? [Y] or [N]"
    }
    if ($answer -eq "n") {
        Read-Host "Quitting...`nPress any key to exit"
        exit
    }
    if ($answer -eq "y") {
        Write-Host "`nFormatting all USB drives..."
        # This should prevent the Explorer GUI pop-up prompting to format
        Stop-Service -Name ShellHWDetection
        $usb | Clear-Disk -RemoveData -Confirm:$false -PassThru | New-Partition -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem FAT32 -NewFileSystemLabel $label | Out-Null
        Start-Service -Name ShellHWDetection
        Write-Host "Done formatting"
    }
}

$drives = Get-Volume -FileSystemLabel $label
Sync-Drives -Letters $drives.DriveLetter -SyncSource $source
Read-Host "`nPress any key to exit"
