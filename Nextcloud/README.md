# Nexcloud scripts

## nc-s3-sync
This script backs up the files of all your Nextcloud users to an s3 bucket. 
It is useful if you host Nexcloud on your own hardware and want some type of disaster 
recovery. By default it uses the **DEEP_ARCHIVE** storage tier, but you can change it 
to suit your needs. Ideally, you would never have to restore anything from the bucket, 
but hard drives are mortal, and sh*t happens.
